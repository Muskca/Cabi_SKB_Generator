/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cabi_skb_generator;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.jena.atlas.web.ContentType;
import org.apache.jena.graph.Graph;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Fabien
 */
public class CABI_SKB_generator {
    
    
    public static HashMap<String, String> classMappings;
    public static String baseUri = "http://ontology.irstea.fr/agronomictaxon/SKB/CABI#";
    public static String taxonomyUri = baseUri+"CABI_Thesaurus";
    
    
    public static String cleanString(String s){
        return s.replaceAll(" ", "_").replaceAll("\\.", "");
    }
    
    public static String generateTriple(String subject, String predicate, String object){
        String ret = "<"+subject+">";
        if(predicate.equals("a")){
            String type = classMappings.get(object);
            boolean newClass= false;
            if(type == null){
                type = baseUri+cleanString(object.toLowerCase().substring(0, object.indexOf(" ")));
                newClass = true;
            }
            ret += " a <"+type+">.\n";
            if(newClass){
                ret += "<"+type+"> <http://www.w3.org/2000/01/rdf-schema#subClassOf> <http://ontology.irstea.fr/agronomictaxon/core#Taxon>.\n";
            }
        }
        else if(predicate.equals("rdfs:label")){
            ret += " <http://www.w3.org/2000/01/rdf-schema#label> "+object+".\n";
        }
        else{
            ret += " "+predicate+" ";
            if(object.startsWith("http"))
                ret += "<"+object+">.\n";
            else
                ret += object+". \n";
        }
        
        
        return ret;
    }
    
     public static HttpEntity graphToHttpEntity(final Graph graph) {
        final RDFFormat syntax = RDFFormat.TURTLE_BLOCKS ;
        ContentProducer producer = new ContentProducer() {
            @Override
            public void writeTo(OutputStream out) {
                RDFDataMgr.write(out, graph, syntax) ;
            }
        } ;
        EntityTemplate entity = new EntityTemplate(producer) ;
        ContentType ct = syntax.getLang().getContentType() ;
        entity.setContentType(ct.getContentType()) ;
        return entity ;
    }
    
     public static StringBuilder concatUpperTaxon(String url, String urlLimit, boolean first){
          StringBuilder ret = new StringBuilder();
         try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            Elements links = doc.select("div.term b");
            String termString = links.first().text();
            //System.out.println("TERM : "+termString);
            String taxonUri = baseUri+cleanString(termString);
            ArrayList<String> upper = new ArrayList<>();
            String state = null;
            links = doc.select("dl dt, dl dd");
            StringBuilder tempRet = new StringBuilder("");
            tempRet.append("<"+taxonomyUri+"> <http://ontology.irstea.fr/AgronomicTaxon#memberScheme> <"+taxonUri+">.\n");
            tempRet.append("<"+taxonUri+"> <http://www.w3.org/2004/02/skos/core#inScheme> <"+taxonomyUri+">.\n");
            tempRet.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+url+">.\n");
            for(Element e : links){
                System.out.println("test : "+e.text()+" -- "+e.nodeName());
                if(e.nodeName() == "dt"){
                    if(e.text().equalsIgnoreCase("Taxonomic Rank")){
                        state = "a";
                    }
                    else if(e.text().equalsIgnoreCase("Broader Term")){
                        state = "broader";
                    }
                    else if(e.text().equalsIgnoreCase("Narrower Term")){
                        state = "narrower";
                    }
                    else if(e.text().equalsIgnoreCase("Related term")){
                        state = "alternativeLabel";
                    }
                    else if(e.text().equalsIgnoreCase("Français") || e.text().equalsIgnoreCase("Deutsch") || e.text().equalsIgnoreCase("Español")){
                        state = "label("+e.text().substring(0, 2).toLowerCase()+")";
                    }
                    else if(e.text().equalsIgnoreCase("Technical Category")){
                        state = "tech";
                    }
                    else if(e.text().equalsIgnoreCase("Subject Category")){
                        state = "subject";
                    }
                    else{
                        state = null;
                    }
                }
                else if(e.nodeName() == "dd"){
                    if(state != null){
//                        if(state.equalsIgnoreCase("narrower")){
//                            Document narDoc = Jsoup.parse(e.html());
//                            Element aElem = narDoc.select("a").first();
//                            String objectUri = baseUri+cleanString(aElem.text());
//                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", objectUri));
//                        }
                        if(state.equalsIgnoreCase("broader")){
                            Document narDoc = Jsoup.parse(e.html());
                            Element aElem = narDoc.select("a").first();
                            upper.add("http://www.cabi.org"+aElem.attr("href"));
                            String objectUri = baseUri+cleanString(aElem.text());
                            if(!url.equalsIgnoreCase(urlLimit))
                                tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>", objectUri));
                                tempRet.append(generateTriple(objectUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", taxonUri));
                        }
                        else if(state.equalsIgnoreCase("a")){
                            //String objectUri = baseUri+"/Taxon/"+cleanString(e.text());
                            tempRet.append(generateTriple(taxonUri, "a", e.text()));
                        }
                        else if(state.equalsIgnoreCase("alternativeLabel")){
                            tempRet.append(generateTriple(taxonUri, "rdfs:label", "\""+e.text()+"\""));
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasVernacularName>", "\""+e.text()+"\""));
                        }
                        else if(state.indexOf("label") == 0){
                            tempRet.append(generateTriple(taxonUri, "rdfs:label", "\""+e.text()+"\"@"+state.substring(6,8)));
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasScientificName>", "\""+e.text()+"\"@"+state.substring(6,8)));
                        }
                        else if(state.equalsIgnoreCase("tech")){
                            if(!e.text().contains("SCI Scientific Name")){
                                System.out.println("ERROR NOT CONTAIN SCI ! "+url);
                                return new StringBuilder();
                            }
                        }
                        else if(state.equalsIgnoreCase("subject")){
                            if(!e.text().contains("ON Organism Names")){
                                System.out.println("ERROR NOT CONTAIN ON ! "+url);
                                return new StringBuilder();
                            }
                        }
                    }
                }
            }
            if(!first)
                ret.append(tempRet);
            if(!url.equalsIgnoreCase(urlLimit)){
                for(String up : upper){
                    ret.append(concatUpperTaxon(up, urlLimit, false));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CABI_SKB_generator.class.getName()).log(Level.SEVERE, null, ex);
        }
         return ret;
     }
     
    public static StringBuilder concatTaxon(String url){
        StringBuilder ret = new StringBuilder();
         try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            Elements links = doc.select("div.term b");
            String termString = links.first().text();
            //System.out.println("TERM : "+termString);
            String taxonUri = baseUri+cleanString(termString);
            ArrayList<String> narrower = new ArrayList<>();
            String state = null;
            links = doc.select("dl dt, dl dd");
            StringBuilder tempRet = new StringBuilder("");
            tempRet.append("<"+taxonomyUri+"> <http://ontology.irstea.fr/AgronomicTaxon#memberScheme> <"+taxonUri+">.\n");
            tempRet.append("<"+taxonUri+"> <http://www.w3.org/2004/02/skos/core#inScheme> <"+taxonomyUri+">.\n");
            tempRet.append("<"+taxonUri+"> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <"+url+">.\n");
            for(Element e : links){
                System.out.println("test : "+e.text()+" -- "+e.nodeName());
                if(e.nodeName() == "dt"){
                    if(e.text().equalsIgnoreCase("Taxonomic Rank")){
                        state = "a";
                    }
                    else if(e.text().equalsIgnoreCase("Broader Term")){
                        state = "broader";
                    }
                    else if(e.text().equalsIgnoreCase("Narrower Term")){
                        state = "narrower";
                    }
                    else if(e.text().equalsIgnoreCase("Related term")){
                        state = "alternativeLabel";
                    }
                    else if(e.text().equalsIgnoreCase("Français") || e.text().equalsIgnoreCase("Deutsch") || e.text().equalsIgnoreCase("Español")){
                        state = "label("+e.text().substring(0, 2).toLowerCase()+")";
                    }
                    else if(e.text().equalsIgnoreCase("Technical Category")){
                        state = "tech";
                    }
                    else if(e.text().equalsIgnoreCase("Subject Category")){
                        state = "subject";
                    }
                    else{
                        state = null;
                    }
                }
                else if(e.nodeName() == "dd"){
                    if(state != null){
                        if(state.equalsIgnoreCase("narrower")){
                            Document narDoc = Jsoup.parse(e.html());
                            Element aElem = narDoc.select("a").first();
                            narrower.add("http://www.cabi.org"+aElem.attr("href"));
                            String objectUri = baseUri+cleanString(aElem.text());
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasLowerRank>", objectUri));
                        }
                        else if(state.equalsIgnoreCase("broader")){
                            Document narDoc = Jsoup.parse(e.html());
                            Element aElem = narDoc.select("a").first();
                            String objectUri = baseUri+cleanString(aElem.text());
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasHigherRank>", objectUri));
                        }
                        else if(state.equalsIgnoreCase("a")){
                            //String objectUri = baseUri+"/Taxon/"+cleanString(e.text());
                            tempRet.append(generateTriple(taxonUri, "a", e.text()));
                        }
                        else if(state.equalsIgnoreCase("alternativeLabel")){
                            tempRet.append(generateTriple(taxonUri, "rdfs:label", "\""+e.text()+"\""));
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasVernacularName>", "\""+e.text()+"\""));
                        }
                        else if(state.indexOf("label") == 0){
                            tempRet.append(generateTriple(taxonUri, "rdfs:label", "\""+e.text()+"\"@"+state.substring(6,8)));
                            tempRet.append(generateTriple(taxonUri, "<http://ontology.irstea.fr/agronomictaxon/core#hasScientificName>", "\""+e.text()+"\"@"+state.substring(6,8)));
                        }
                        else if(state.equalsIgnoreCase("tech")){
                            if(!e.text().contains("SCI Scientific Name")){
                                System.out.println("ERROR NOT CONTAIN SCI ! "+url);
                                return new StringBuilder();
                            }
                        }
                        else if(state.equalsIgnoreCase("subject")){
                            if(!e.text().contains("ON Organism Names")){
                                System.out.println("ERROR NOT CONTAIN ON ! "+url);
                                return new StringBuilder();
                            }
                        }
                    }
                }
            }
            ret.append(tempRet);
            for(String nar : narrower){
                ret.append(concatTaxon(nar));
            }
        } catch (IOException ex) {
            Logger.getLogger(CABI_SKB_generator.class.getName()).log(Level.SEVERE, null, ex);
        }
         return ret;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       classMappings = new HashMap<>();
       classMappings.put("Genus Genus", "http://ontology.irstea.fr/agronomictaxon/core#GenusRank");
       classMappings.put("Species Species", "http://ontology.irstea.fr/agronomictaxon/core#SpecyRank");
       classMappings.put("Family Family", "http://ontology.irstea.fr/agronomictaxon/core#FamilyRank");
       classMappings.put("Order Order", "http://ontology.irstea.fr/agronomictaxon/core#OrderRank");
       classMappings.put("Unranked Unranked", "http://ontology.irstea.fr/agronomictaxon/core#Taxon");
        
        StringBuilder out = new StringBuilder();
        System.out.println("Export ontological module prefixes ... ");
        try {
            String modulePrefixes = FileUtils.readFileToString(new File("in/agronomicTaxon_prefix.ttl"), "utf-8");
            out.append(modulePrefixes);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Prefixes exported");
        
        System.out.println("Export ontological module ... ");
        try {
            String moduleTriples = FileUtils.readFileToString(new File("in/agronomicTaxon_2.ttl"), "utf-8");
            out.append(moduleTriples);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Module exported");
        
        
       out.append("<"+baseUri+"> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Ontology>.\n");
       
       out.append("<"+taxonomyUri+"> a <http://ontology.irstea.fr/agronomictaxon/core#Taxonomy>.\n");
       out.append(concatUpperTaxon("http://www.cabi.org/cabthesaurus/mtwdk.exe?k=default&l=60&w=13090&n=1&s=5&t=2", "http://www.cabi.org/cabthesaurus/mtwdk.exe?k=default&l=60&w=2881&n=1&s=5&t=2", true));
       StringBuilder concat = concatTaxon("http://www.cabi.org/cabthesaurus/mtwdk.exe?k=default&l=60&w=13090&n=1&s=5&t=2");
       out.append(concat);
       System.out.println(out);
        try {
            FileUtils.write(new File("out_CABI.ttl"), out);
        } catch (IOException ex) {
            Logger.getLogger(CABI_SKB_generator.class.getName()).log(Level.SEVERE, null, ex);
        }
//       DatasetGraph dsg = DatasetGraphFactory.create();
//        DataService dataService = new DataService(dsg) ;
//        dataService.addEndpoint(OperationName.Query, "");
//        dataService.addEndpoint(OperationName.Update, "");
//
//        FusekiEmbeddedServer server = FusekiEmbeddedServer.create()
//           .setPort(3332)
//           .add("/CABI", dataService)
//           .build() ;
//        server.start() ;
//
//        Graph g = SSE.parseGraph("(graph "+ret+")") ;
//        HttpEntity e = graphToHttpEntity(g) ;
//        HttpOp.execHttpPut("http://localhost:3332/CABI", e) ;
        
    }
}
